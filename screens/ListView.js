
import React, { Component } from 'react';
import { ListView, Text } from "react-native";

class ListViewPage extends Component {
    constructor() {
      super();
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      this.state = {
        dataSource: ds.cloneWithRows(['Row 1 - Example data', 'Row 2 - Another example data','Row 3 - Dunt dunt duuh..']),
      };
    }
  
    render() {
      return (
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) => <Text>{rowData}</Text>}
        />
      );
    }
  }

export default ListViewPage;


