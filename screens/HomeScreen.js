import React, { Component } from "react";
import{
    View,Text,StyleSheet,Button,ScrollView
} from "react-native";

class HomeScreen extends Component{
    render(){
        return(
            <ScrollView>
            <View>
            <Text> </Text>
                <Button style={styles.butt} onPress={() => this.props.navigation.navigate('ActivityIndicator')} title="ActivityIndicator"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('Button')} title="Button"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('DrawerLayout')} title="Drawer Layer"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('FlatList')} title="FlatList"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('Image')} title="Image"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('KeyboardAvoidingView')} title="Keyboard Avoiding Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('ListView')} title="List view Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('Modal')} title="Modal Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('Picker')} title="Picker Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('ProgressBar')} title="Progress bar Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('RefreshControl')} title="Refresh control Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('SafeArea')} title="Safe area Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('ScrollViewPage')} title="Scroll View Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('SectionList')} title="Section List Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('StatusBar')} title="Status bar Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('Switch')} title="Switch Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('Text_TextInp')} title="Text and Text Input Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('TouchableHighlight')} title="TouchableHighlight"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('ViewPage')} title="View Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('PageAndroid')} title="View Page Android Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('WebView')} title="Web View Component"></Button>
            <Text> </Text>

            </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    butt: {
        borderRadius: 10,
        backgroundColor: '#841584'
    }
})

export default HomeScreen;