import React, { Component } from 'react';
import { Picker, StyleSheet} from "react-native";

class PickerPage extends Component {

    constructor(){
        super();
        this.state={
            language:''
        }
    }

    render() {
        return (
            <Picker
            selectedValue={this.state.language}
            style={{ height: 50, width: 100 }}
            onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
            <Picker.Item label="Java" value="java" />
            <Picker.Item label="JavaScript" value="js" />
            </Picker>
        );
    }
}

export default PickerPage;