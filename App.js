import React from 'react';
import { 
  StyleSheet,Text,View,
} from 'react-native';

import {createStackNavigator} from 'react-navigation'

import HomeScreen from "./screens/HomeScreen"
import ActivityIndicator from "./screens/ActivityIndicator"
import Button from "./screens/Button"
import DrawerLayout from "./screens/DrawerLayout"
import FlatList from "./screens/FlatList"
import Image from "./screens/Image"
import KeyboardAvoidingView from "./screens/KeyboardAvoidingView"
import ListView from "./screens/ListView"
import Modal from "./screens/Modal"
import Picker from "./screens/Picker"
import ProgressBar from "./screens/ProgressBar"
import RefreshControl from "./screens/RefreshControl"
import SafeArea from "./screens/SafeArea"
import ScrollViewPage from "./screens/ScrollViewPage"
import SectionList from "./screens/SectionList"
import StatusBar from "./screens/StatusBar"
import Switch from "./screens/Switch"
import Text_TextInp from "./screens/Text_TextInp"
import TouchableHighlight from "./screens/TouchableHighlight"
import ViewPage from "./screens/ViewPage"
import PageAndroid from "./screens/PageAndroid"
import WebView from "./screens/WebView"


export default class App extends React.Component {
  render() {
    return (
      <AppNavigator/>
    );
  }
}


const AppNavigator = createStackNavigator({

 
  HomeScreen: { screen: HomeScreen},
  ActivityIndicator: {screen: ActivityIndicator},
  Button: {screen: Button},
  DrawerLayout: {screen: DrawerLayout},
  FlatList: {screen: FlatList},
  Image: {screen: Image},
  KeyboardAvoidingView: {screen: KeyboardAvoidingView},
  ListView: {screen: ListView},
  Modal: {screen: Modal},
  Picker: {screen: Picker},
  ProgressBar: {screen: ProgressBar},
  RefreshControl: {screen: RefreshControl},
  SafeArea: {screen: SafeArea},
  ScrollViewPage: {screen: ScrollViewPage},
  SectionList: {screen: SectionList},
  StatusBar: {screen: StatusBar},
  Switch: {screen: Switch},
  Text_TextInp: {screen: Text_TextInp},
  TouchableHighlight: {screen: TouchableHighlight},
  ViewPage: {screen: ViewPage},
  PageAndroid: {screen: PageAndroid},
  WebView: {screen: WebView}

})
;
